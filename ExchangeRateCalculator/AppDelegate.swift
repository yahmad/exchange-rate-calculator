//
//  AppDelegate.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

