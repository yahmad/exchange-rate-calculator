//
//  CurrencyFormatter.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

class CurrencyFormatter: NumberFormatter {
    
    static let standard = CurrencyFormatter()
    
    init(minimumFractionDigits: Int = 2, maximumFractionDigits: Int = 2) {
        super.init()
        self.alwaysShowsDecimalSeparator = minimumFractionDigits > 0
        self.numberStyle = .decimal
        self.minimumFractionDigits = minimumFractionDigits
        self.maximumFractionDigits = maximumFractionDigits
    }
    
    func formatAsCurrency(decimal: Decimal) -> String? {
        return string(from: decimal as NSDecimalNumber)
    }
    
    func formatAsCurrency(_ value: Int) -> String? {
        return string(from: NSNumber(integerLiteral: value))
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
