//
//  Loadable.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

enum Loadable<T> {
    case loading
    case loaded(T)
    
    var value: T? {
        switch self {
        case .loaded(let value): return value
        case .loading: return nil
        }
    }
}

extension Loadable: Equatable where T: Equatable {}
