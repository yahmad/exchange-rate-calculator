//
//  FixerAPI.swift
//  Networking
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation
import Networking

struct FixerConfiguration: Decodable {
    let accessKey: String
    let baseURL: String
}

class FixerAPI: NetworkAPIProvider {
    
    lazy var configuration: FixerConfiguration? = {
        if let path = Bundle.main.path(forResource: "FixerConfiguration", ofType: "plist"),
            let xml = FileManager.default.contents(atPath: path) {
            return try? PropertyListDecoder().decode(FixerConfiguration.self, from: xml)
        }
        return nil
    }()
    
    var baseURL: URL {
        guard
            let rawURL = configuration?.baseURL,
            let url = URL(string: rawURL)
            else {
                fatalError("Failed to parse configuration file")
        }
        return url
    }
    
    var queryParams: [String : String] {
        guard let accessKey = configuration?.accessKey else {
            fatalError("Failed to parse configuration file")
        }
        return ["access_key": accessKey]
    }
}
