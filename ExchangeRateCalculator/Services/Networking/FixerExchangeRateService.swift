//
//  FixerService.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation
import Networking
import Domain

class FixerExchangeRateService: ExchangeRateService {

    enum ServiceError: Swift.Error {
        case unexpectedDateFormat
    }

    private let networkService: NetworkProvider
    private let dateFormatter: DateFormatter
    
    init(networkService: NetworkProvider = NetworkService(api: FixerAPI()),
         dateFormatter: DateFormatter = .fixerDateFormatter) {
        self.networkService = networkService
        self.dateFormatter = dateFormatter
    }
    
    func fetchLatestExchangeRates(for currencyCodes: [CurrencyCode], base baseCurrencyCode: CurrencyCode, completion: @escaping (Result<Domain.ExchangeRates, Swift.Error>) -> Void) {
        let request = LatestExchangeRatesRequest(
            base: baseCurrencyCode,
            currencyCodes: currencyCodes
        )
        networkService.make(request: request, completion: { [dateFormatter] (result: Result<FixerLatestRatesResponseBody, Error>) in
            completion(result.flatMap({ response in
                guard let date = dateFormatter.date(from: response.date) else {
                    return .failure(ServiceError.unexpectedDateFormat)
                }
                return .success(Domain.ExchangeRates(date: date, base: response.base, rates: response.rates))
            }))
        })
    }
    
    func fetchHistoricalRates(for currencyCodes: [CurrencyCode], base baseCurrencyCode: CurrencyCode, date: Date, completion: @escaping (Result<Domain.ExchangeRates, Swift.Error>) -> Void) {
        let request = HistoricalExchangeRatesRequest(
            base: baseCurrencyCode,
            currencyCodes: currencyCodes,
            date: date
        )
        networkService.make(request: request, completion: { [dateFormatter] (result: Result<FixerHistoricalExchangeRatesResponseBody, Error>) in
            completion(result.flatMap({ response -> Result<Domain.ExchangeRates, Swift.Error> in
                guard let date = dateFormatter.date(from: response.date) else {
                    return .failure(ServiceError.unexpectedDateFormat)
                }
                return .success(Domain.ExchangeRates(date: date, base: response.base, rates: response.rates))
            }))
        })
    }
}
