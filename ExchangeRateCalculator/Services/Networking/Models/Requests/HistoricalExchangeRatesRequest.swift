//
//  HistoricalExchangeRatesRequest.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation
import Networking
import Domain

struct HistoricalExchangeRatesRequest: NetworkRequest {
    
    private let base: CurrencyCode
    private let currencyCodes: [CurrencyCode]
    private let date: Date
    
    init(base: CurrencyCode, currencyCodes: [CurrencyCode], date: Date) {
        self.base = base
        self.currencyCodes = currencyCodes
        self.date = date
    }
    
    var endpoint: String {
        return formatted(date: self.date)
    }
    
    var method: HTTPMethod = .GET
    
    var queryParams: [String : String] {
        return [
            "base": base,
            "symbols": currencyCodes.joined(separator: ",")
        ]
    }
    
    func formatted(date: Date) -> String {
        return DateFormatter.fixerDateFormatter.string(from: date)
    }
}
