//
//  LatestExchangeRatesRequest.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation
import Networking
import Domain

struct LatestExchangeRatesRequest: NetworkRequest {
    
    private let base: CurrencyCode
    private let currencyCodes: [CurrencyCode]
    
    init(base: CurrencyCode, currencyCodes: [CurrencyCode]) {
        self.base = base
        self.currencyCodes = currencyCodes
    }
    
    var endpoint: String = "latest"
    
    var method: HTTPMethod = .GET
    
    var queryParams: [String : String] {
        return [
            "base": base,
            "symbols": currencyCodes.joined(separator: ",")
        ]
    }
}
