//
//  FixerLatestRatesResponseBody.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

struct FixerLatestRatesResponseBody: Decodable {
    let base: String
    let date: String
    let rates: [String: Decimal]
}
