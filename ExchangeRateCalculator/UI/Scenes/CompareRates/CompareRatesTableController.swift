//
//  CompareRatesTableController.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import UIKit

class CompareRatesTableController: NSObject {
    
    weak var tableView: UITableView?
    
    var cellViewModels: [CurrencyRateComparisonTableViewCell.ViewModel] = [] {
        didSet {
            tableView?.reloadData()
        }
    }
    
    init(tableView: UITableView) {
        super.init()
        self.tableView = tableView
    }
}

extension CompareRatesTableController: UITableViewDataSource {
    
    static let cellIdentifer = "CurrencyRateComparisonCell"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: self).cellIdentifer, for: indexPath)
        if let cell = cell as? CurrencyRateComparisonTableViewCell {
            cell.viewModel = cellViewModels[indexPath.row]
        }
        return cell
    }
}

extension CompareRatesTableController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
