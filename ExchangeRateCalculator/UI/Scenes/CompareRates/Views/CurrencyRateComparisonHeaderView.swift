//
//  CurrencyRateComparisonHeaderView.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import UIKit

class CurrencyRateComparisonHeaderView: UIView {
    
    struct ViewModel {
        let dateTitle: String
        let leftCurrencySymbol: String
        let rightCurrencySymbol: String
    }

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var leftCurrencyTitleLabel: UILabel!
    @IBOutlet weak var rightCurrencyTitleLabel: UILabel!
    
    var viewModel: ViewModel? {
        didSet {
            updateContent()
        }
    }
    
    @available(*, unavailable)
    override init(frame: CGRect) {
        fatalError("Not implemented")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        updateContent()
    }
    
    private func updateContent() {
        dateLabel.text = viewModel?.dateTitle
        leftCurrencyTitleLabel.text = viewModel?.leftCurrencySymbol
        rightCurrencyTitleLabel.text = viewModel?.rightCurrencySymbol
    }
}
