//
//  CurrencyRateComparisonTableViewCell.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import UIKit

class CurrencyRateComparisonTableViewCell: UITableViewCell {
    
    struct ViewModel {
        let formattedDate: String
        let leftRateText: String
        let rightRateText: String
    }
    
    var viewModel: ViewModel? {
        didSet {
            updateContent()
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var leftRateLabel: UILabel!
    @IBOutlet weak var rightRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateContent()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func updateContent() {
        dateLabel.text = viewModel?.formattedDate
        leftRateLabel.text = viewModel?.leftRateText
        rightRateLabel.text = viewModel?.rightRateText
    }
}
