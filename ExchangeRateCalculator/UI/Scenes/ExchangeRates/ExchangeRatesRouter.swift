//
//  ExchangeRatesRouter.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright (c) 2020 Yasir Ahmad. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol ExchangeRatesRoutingLogic {
    func routeToCurrencyComparison(segue: UIStoryboardSegue?)
}

protocol ExchangeRatesDataPassing {
    var dataStore: ExchangeRatesDataStore { get }
}

class ExchangeRatesRouter: NSObject, ExchangeRatesRoutingLogic, ExchangeRatesDataPassing {
    weak var viewController: ExchangeRatesViewController?
    let dataStore: ExchangeRatesDataStore
    
    init(viewController: ExchangeRatesViewController, dataStore: ExchangeRatesDataStore) {
        self.viewController = viewController
        self.dataStore = dataStore
    }
    
    func routeToCurrencyComparison(segue: UIStoryboardSegue?) {
        if let segue = segue {
            let destinationVC = segue.destination as! CompareRatesViewController
            var destinationDS = destinationVC.router!.dataStore
            passDataToCurrencyComparison(source: dataStore, destination: &destinationDS)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let destinationVC = storyboard.instantiateViewController(withIdentifier: "CompareRatesViewController") as! CompareRatesViewController            
            var destinationDS = destinationVC.router!.dataStore
            passDataToCurrencyComparison(source: dataStore, destination: &destinationDS)
            viewController?.show(destinationVC, sender: nil)
        }
    }
    
    private func passDataToCurrencyComparison(source: ExchangeRatesDataStore, destination: inout CompareRatesDataStore) {
        destination.baseCurrencyCode = source.baseCurrencyCode
        destination.amount = source.inputAmount
        destination.selectedCurrencyCodes = source.selectedCurrencyCodes
    }
}
