//
//  CurrencyRateTableViewCell.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import UIKit

class CurrencyRateTableViewCell: UITableViewCell {
    
    struct ViewModel {
        let currencySymbol: String
        let rateText: String
    }

    @IBOutlet weak var currencyCodeLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    var viewModel: ViewModel? {
        didSet {
            updateContent()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateContent()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func updateContent() {
        currencyCodeLabel.text = viewModel?.currencySymbol
        rateLabel.text = viewModel?.rateText
    }
}
