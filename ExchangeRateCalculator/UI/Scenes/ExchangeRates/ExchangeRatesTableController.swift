//
//  ExchangeRatesTableController.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import UIKit
import Domain

protocol ExchangeRatesTableControllerDelegate: class {
    func selected(currencies: [CurrencyCode])
}

class ExchangeRatesTableController: NSObject {
    
    weak var tableView: UITableView?
    weak var delegate: ExchangeRatesTableControllerDelegate?
    
    var cellViewModels: [CurrencyRateTableViewCell.ViewModel] = [] {
        didSet {
            tableView?.reloadData()
        }
    }
    
    init(tableView: UITableView, delegate: ExchangeRatesTableControllerDelegate) {
        super.init()
        self.tableView = tableView
        self.delegate = delegate
    }
}

extension ExchangeRatesTableController: UITableViewDataSource {
    
    static let cellIdentifer = "CurrencyRateCell"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: self).cellIdentifer, for: indexPath)
        if let cell = cell as? CurrencyRateTableViewCell {
            cell.viewModel = cellViewModels[indexPath.row]
        }
        return cell
    }
}

extension ExchangeRatesTableController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCurrencies = tableView.indexPathsForSelectedRows?.map { cellViewModels[$0.row].currencySymbol } ?? []
        delegate?.selected(currencies: selectedCurrencies)
    }
}
