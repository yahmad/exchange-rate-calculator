//
//  UIViewController+Ext.swift
//  ExchangeRateCalculator
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import UIKit

protocol Alertable {
    func showAlert(message: String, completion: (() -> Void)?)
}

extension Alertable where Self: UIViewController {
    func showAlert(message: String, completion: (() -> Void)?) {
        let alert = UIAlertController(
            title: "Error",
            message: message,
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion?()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
