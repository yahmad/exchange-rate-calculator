//
//  ExchangeRateDateSeries.swift
//  Domain
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

public struct ExchangeRateDateSeries: Equatable {
    public let baseCurrencyCode: CurrencyCode
    public let ratesByDates: [Date: CurrencyRates]
    
    init(base: CurrencyCode, ratesByDates: [Date: CurrencyRates]) {
        self.baseCurrencyCode = base
        self.ratesByDates = ratesByDates
    }
}
