//
//  Currency.swift
//  Domain
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

public typealias CurrencyCode = String

/* Considered using an enum here, but the list is likely to change
 To keep to the Open-Closed SOLID principle, enums should generally not be expected to change */
public extension CurrencyCode {
    static let USDollar = "USD"
    static let Euro = "EUR"
    static let JapaneseYen = "JPY"
    static let BritishPound = "GBP"
    static let AustralianDollar = "AUD"
    static let CanadianDollar = "CAD"
    static let SwissFranc = "CHF"
    static let ChineseYuan = "CNY"
    static let SwedishKrona = "SEK"
    static let NewZealandDollar = "NZD"
}

public struct Currency: Equatable {
    let name: String
    let code: CurrencyCode
}
