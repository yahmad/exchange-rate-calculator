//
//  ExchangeRates.swift
//  Domain
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

public typealias CurrencyRates = [CurrencyCode: Decimal]

public struct ExchangeRates: Equatable {
    public let date: Date
    public let baseCurrencyCode: CurrencyCode
    public let rates: CurrencyRates
    
    public init(date: Date, base: CurrencyCode, rates: CurrencyRates) {
        self.date = date
        self.baseCurrencyCode = base
        self.rates = rates
    }
}
