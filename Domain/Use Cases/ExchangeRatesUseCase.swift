//
//  ExchangeRatesUseCase.swift
//  Domain
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation
import Networking

public protocol ExchangeRatesUseCaseProtocol {
    func latestExchangeRates(for currencyCodes: [CurrencyCode], base: CurrencyCode, completion: @escaping ((Result<ExchangeRates, Error>) -> Void))
    func historicalRates(for currencyCodes: [CurrencyCode], base: CurrencyCode, numberOfDays: Int, completion: @escaping ((Result<ExchangeRateDateSeries, Error>) -> Void))
}

public class ExchangeRatesUseCase: ExchangeRatesUseCaseProtocol {
    
    enum HistoricalRatesError: Swift.Error {
        case invalidNumberOfDays
    }
    
    private let exchangeRateService: ExchangeRateService
    
    public init(exchangeRateService: ExchangeRateService) {
        self.exchangeRateService = exchangeRateService
    }
    
    public func latestExchangeRates(for currencyCodes: [CurrencyCode], base: CurrencyCode, completion: @escaping ((Result<ExchangeRates, Error>) -> Void)) {
        exchangeRateService.fetchLatestExchangeRates(for: currencyCodes, base: base, completion: completion)
    }
    
    // Not the ideal implementation, but the "time-series" Fixer.io endpoint is only available on the paid plan
    // I have used the historical data endpoint to fetch the required data, but would not use this approach in production.
    public func historicalRates(for currencyCodes: [CurrencyCode], base: CurrencyCode, numberOfDays: Int, completion: @escaping ((Result<ExchangeRateDateSeries, Error>) -> Void)) {
        guard numberOfDays > 0 else {
            completion(.failure(HistoricalRatesError.invalidNumberOfDays))
            return
        }
        
        let today = Date()
        let dates = (0..<numberOfDays).compactMap { offset -> Date? in
            return Calendar.current.date(byAdding: .day, value: -offset, to: today)
        }
        
        var ratesByDates: [Date: CurrencyRates] = [:]
        
        dates.forEach { date in
            exchangeRateService.fetchHistoricalRates(for: currencyCodes, base: base, date: date, completion: { result in
                switch result {
                case .success(let rates):
                    ratesByDates[rates.date] = rates.rates
                    if ratesByDates.count == numberOfDays {
                        completion(.success(ExchangeRateDateSeries(base: base, ratesByDates: ratesByDates)))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            })
        }
    }
}
