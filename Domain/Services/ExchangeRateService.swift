//
//  ExchangeRateService.swift
//  Networking
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

public protocol ExchangeRateService {
    func fetchLatestExchangeRates(for currencyCodes: [CurrencyCode], base baseCurrencyCode: CurrencyCode, completion: @escaping (Result<Domain.ExchangeRates, Error>) -> Void)
    func fetchHistoricalRates(for currencyCodes: [CurrencyCode], base baseCurrencyCode: CurrencyCode, date: Date, completion: @escaping (Result<Domain.ExchangeRates, Error>) -> Void)
}
