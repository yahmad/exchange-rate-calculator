#  Submission Notes


## Assumptions

- Currencies may be added/removed in the future
- Supporting minimum iOS 12 deployment target
    - In most cases, I would choose to support the current and previous major iOS versions
- The "time-series" Fixer.io endpoint would be most appropriate for the comparison view, but is not available on the free tier
- The user can only enter integer values (unlikely that a person needs to know the exchange rate to the penny)


## Time Keeping

I spent ~30 minutes before creating this project setting up my Fixer account and considering dependencies and architecture approach.
I initially considered trying to timebox this to 3-4 hours, though likely spent ~6-7 hours working sporadically through the day. 


## Running the application

You will need to change the team to a valid one under the Project Settings "Signing" tab (also for the two frameworks)


## Design Decisions

### Architecture

Using a "Clean Swift" based approach, as I have a template "Scene" generator that allows me to quickly generate the required files for a UI.

This architecture is somewhat heavy, and I considered using MVVM initially, but decided against that due to a few reasons:
- there are many MVVM "flavours", and it is easy to adopt an approach where you move from a massive view controller to a massive 
view model layer (or at least be breaking the Single Resposibility Principle).
- I have most recently been using Clean Swift, and decided to use this for familiarity and to keep to time constraints.


### UI

I am using Autolayout with Storyboards enabled. I have not used SwiftUI or Combine, as I have not yet spent much
time learning the best approach to using them (will likely wait until WWDC to give them a year for them to "stabilise").


### Dependencies

Using Alamofire for expediency, rather than building a custom Networking layer.
NSURLSession is fairly easy to use directly, and I would consider using it directly in an application.  


### Approach

I will use a top-down approach, starting with the UI. As I have no designs, creating the UI first will allow me to have 
an idea of the data requirements. Starting from the networking or model layer may result in having to go back if I 
make an incorrect presumption.

I will mock out use cases (business logic) as required, and replace when appropriate.


## Improvements

- Ran over intended time, would have added more unit tests (focussing on business logic in scenes and use cases)
- Localisation
