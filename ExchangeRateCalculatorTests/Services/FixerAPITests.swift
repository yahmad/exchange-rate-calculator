//
//  FixerAPITests.swift
//  ExchangeRateCalculatorTests
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import XCTest
@testable import ExchangeRateCalculator

class FixerAPITests: XCTestCase {
    
    var sut: FixerAPI!
    
    override func setUpWithError() throws {
        sut = FixerAPI()
    }

    func testBaseURL_isNotNil() {
        XCTAssertNotNil(sut.baseURL)
    }

    func testQueryParams_containsAccessKey() {
        let params = sut.queryParams
        XCTAssertNotNil(params)
        XCTAssertNotNil(params["access_key"])
    }
}
