//
//  CurrencyFormatterTests.swift
//  ExchangeRateCalculatorTests
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import XCTest
@testable import ExchangeRateCalculator

class CurrencyFormatterTests: XCTestCase {

    func test_givenZeroMinimumFractions_doesNotAlwaysShowDecimal() {
        let sut = CurrencyFormatter(minimumFractionDigits: 0)
        XCTAssertFalse(sut.alwaysShowsDecimalSeparator)
    }
    
    func test_givenNonZeroMinimumFractions_doesNotAlwaysShowDecimal() {
        let sut = CurrencyFormatter(minimumFractionDigits: 1)
        XCTAssertTrue(sut.alwaysShowsDecimalSeparator)
    }
    
    func test_givenStandardFormatter_whenFormattingInteger_givesTwoDecimalPlaces() {
        let sut = CurrencyFormatter.standard
        let input = 123
        XCTAssertEqual("123.00", sut.formatAsCurrency(input))
    }
    
    func test_givenStandardFormatter_whenFormattingWholeDecimal_givesTwoDecimalPlaces() {
        let sut = CurrencyFormatter.standard
        let input = Decimal(integerLiteral: 123)
        XCTAssertEqual("123.00", sut.formatAsCurrency(decimal: input))
    }
    
    func test_givenStandardFormatter_whenFormattingFractionalDecimalWithTwoDigits_givesTwoDecimalPlaces() {
        let sut = CurrencyFormatter.standard
        let input = Decimal(floatLiteral: 123.45)
        XCTAssertEqual("123.45", sut.formatAsCurrency(decimal: input))
    }
    
    func test_givenStandardFormatter_whenFormattingFractionalDecimalWithManyDigits_shortensToTwoDecimalPlaces() {
        let sut = CurrencyFormatter.standard
        let input = Decimal(floatLiteral: 123.454321234)
        XCTAssertEqual("123.45", sut.formatAsCurrency(decimal: input))
    }
    
    func test_givenStandardFormatter_whenFormattingRoundableFractionalDecimal_roundsToTwoDecimalPlaces() {
        let sut = CurrencyFormatter.standard
        let input = Decimal(floatLiteral: 123.4567)
        XCTAssertEqual("123.46", sut.formatAsCurrency(decimal: input))
    }
}
