//
//  NetworkService.swift
//  Networking
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation
import Alamofire

public protocol NetworkProvider {
    func make<T: Decodable>(request: NetworkRequest, completion: @escaping ((Result<T, Swift.Error>) -> Void))
}

public class NetworkService: NetworkProvider {

    enum Error: Swift.Error {
        case invalidURL
        case missingData
        case failedToDecode
    }
    
    private let api: NetworkAPIProvider
    
    public init(api: NetworkAPIProvider) {
        self.api = api
    }
    
    public func make<T: Decodable>(request: NetworkRequest, completion: @escaping ((Result<T, Swift.Error>) -> Void)) {
        let requestURL = api.baseURL.appendingPathComponent(request.endpoint)
        guard let components = URLComponents(url: requestURL, resolvingAgainstBaseURL: false) else {
            completion(.failure(Error.invalidURL))
            return
        }
                
        let apiQueryParams = api.queryParams.map { (key, value) -> URLQueryItem in
            URLQueryItem(name: key, value: value)
        }
        
        let requestQueryParams = request.queryParams.map({ (key: String, value: String) -> URLQueryItem in
            URLQueryItem(name: key, value: value)
        })
        
        var mutableComponents = components
        mutableComponents.queryItems = apiQueryParams + requestQueryParams
        
        let urlRequest: URLRequest
        do {
            urlRequest = try URLRequest(url: mutableComponents.asURL(), method: Alamofire.HTTPMethod(from: request.method))
        } catch let error {
            completion(.failure(error))
            return
        }
        
        AF.request(urlRequest)
            .validate()
            .response { (response: AFDataResponse<Data?>) in
                let result = response.tryMap { (data) -> T in
                    guard let data = data else {
                        throw Error.missingData
                    }
                    return try JSONDecoder().decode(T.self, from: data)
                }
                completion(result.result)
        }
    }
}
