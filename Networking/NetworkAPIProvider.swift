//
//  NetworkAPIProvider.swift
//  Networking
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

public protocol NetworkAPIProvider {
    var baseURL: URL { get }
    var queryParams: [String: String] { get }
}
