//
//  NetworkRequest.swift
//  Networking
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case PATCH
    case DELETE
}

public protocol NetworkRequest {
    var endpoint: String { get }
    var method: HTTPMethod { get }
    var queryParams: [String: String] { get }
}
