//
//  Alamofire+Ext.swift
//  Networking
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import Foundation
import Alamofire

extension Alamofire.HTTPMethod {
    init(from method: HTTPMethod) {
        switch method {
        case .GET: self = .get
        case .POST: self = .post
        case .PUT: self = .put
        case .PATCH: self = .patch
        case .DELETE: self = .delete
        }
    }
}

