//
//  ExchangeRatesUseCaseTests.swift
//  DomainTests
//
//  Created by Yasir Ahmad on 13/04/2020.
//  Copyright © 2020 Yasir Ahmad. All rights reserved.
//

import XCTest
@testable import Domain


class MockExchangeRateService: ExchangeRateService {
    
    var fetchLatestExchangeRatesWasCalled = false
    func fetchLatestExchangeRates(for currencyCodes: [CurrencyCode], base baseCurrencyCode: CurrencyCode, completion: @escaping (Result<ExchangeRates, Error>) -> Void) {
        fetchLatestExchangeRatesWasCalled = true
    }
    
    var fetchHistoricalRatesWasCalled = false
    var fetchHistoricalRatesCompletionResult: ((Date) -> Result<ExchangeRates, Error>)? = nil
    func fetchHistoricalRates(for currencyCodes: [CurrencyCode], base baseCurrencyCode: CurrencyCode, date: Date, completion: @escaping (Result<ExchangeRates, Error>) -> Void) {
        fetchHistoricalRatesWasCalled = true
        if let result = fetchHistoricalRatesCompletionResult {
            completion(result(date))
        }
    }
}

class ExchangeRatesUseCaseTests: XCTestCase {
    
    var mockExchangeRateService: MockExchangeRateService!
    var sut: ExchangeRatesUseCase!
    
    override func setUpWithError() throws {
        mockExchangeRateService = MockExchangeRateService()
        sut = ExchangeRatesUseCase(exchangeRateService: mockExchangeRateService)
    }
    
    func testLatestExchangeRates() throws {
        sut.latestExchangeRates(for: [], base: "", completion: { _ in })
        XCTAssertTrue(mockExchangeRateService.fetchLatestExchangeRatesWasCalled)
    }
    
    func testHistoricalRates_whenAllFetchRequestsSucceed_thenCompletionCalledWithSuccess() {
        mockExchangeRateService.fetchHistoricalRatesCompletionResult = { date in
            return .success(ExchangeRates(date: date, base: "", rates: [:]))
        }
        let expectation = XCTestExpectation(description: "Completion Handler called with success")
        sut.historicalRates(for: ["ABC"], base: "", numberOfDays: 3, completion: { result in
            if case .success = result {
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testHistoricalRates_whenPartialFetchRequestsFail_thenCompletionCalledWithFailure() {
        let numberOfDays = 3
        var completionCallCount = 0
        
        mockExchangeRateService.fetchHistoricalRatesCompletionResult = { date in
            defer {
                completionCallCount += 1
            }
            if completionCallCount == 0 {
                return .failure(NSError(domain: "ExchangeRatesUseCaseTests", code: 0, userInfo: nil))
            } else {
                return .success(ExchangeRates(date: date, base: "", rates: [:]))
            }
        }
        
        let failureExpectation = XCTestExpectation(description: "Completion Handler called with failure")
        let successExpectation = XCTestExpectation(description: "Completion handler called with success")
        successExpectation.isInverted = true
        
        sut.historicalRates(for: ["ABC"], base: "", numberOfDays: numberOfDays, completion: { result in
            if case .failure = result {
                failureExpectation.fulfill()
            } else {
                successExpectation.fulfill()
            }
        })
        wait(for: [failureExpectation, successExpectation], timeout: 1.0)
    }
}
